**UNItype** 

link al jar :
https://bitbucket.org/gaiaebli/oop19-unitype/raw/072bcdc09075c001f72803b791356c625e54a8f4/oop19-unitype.jar

link alla relazione :
https://bitbucket.org/gaiaebli/oop19-unitype/raw/072bcdc09075c001f72803b791356c625e54a8f4/OOP19-UNItype.pdf

Il nostro team si pone come obiettivo quello di realizzare una propria versione del gioco 
ZType Space Typing & Spelling ,un gioco ideato per diventare più veloci e competenti in dattilografia.
Si basa sulla digitazione di un numero variabile di parole (dipendente dalla difficoltà raggiunta) 
nel minor tempo possibile, prima che queste raggiungano la parte inferiore dello schermo e con la 
maggior precisione.
Obiettivo: realizzare più punti possibili, i punteggi più alti saranno consultabili tramite leaderboard.

Video del gioco originale:  https://youtu.be/liQv7pLTzjo
