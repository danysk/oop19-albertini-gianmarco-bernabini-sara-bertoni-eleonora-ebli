package controller.gui.game;

import controller.core.GameEngineImpl;
import javafx.fxml.FXML;

/**
 * 
 * The controller associated to the game's gui and fxml file. 
 *
 */
public interface GUIGameController {

    /**
     * Returns the game engine of the game.
     * 
     * @return the game engine of the game
     */
    GameEngineImpl getEngine();

    /**
     * Initializes the game.
     */
    void firstTry();

    /**
     * The handler for the click event generated by the 'pause' button.
     */
    @FXML
    void pauseBtnOnClickHandler();
}
